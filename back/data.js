
const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const filename = './text.json';
const filenameComment = './comment.json';
let data = [];
let dataCom = [];

module.exports = {
  async init() {
    try {
      const fileContents = await fs.readFile(filename);
      data = JSON.parse(fileContents.toString());
    } catch (e) {
      data = [];
    }
  },

  getItems() {
    return data;
  },

  getItem(id){
    return data.find(p => p.id === id);
  },

  delItem(id){
   delete data.splice(p => p.id === id,1);
    return this.save();
  },

  addItem(item) {
      item.id = nanoid();
      item.date = new Date().toISOString();
      data.push(item);
      return this.save();
  },

  save() {
    return fs.writeFile(filename, JSON.stringify(data));
  },

  async initCom() {
    try {
      const fileContents = await fs.readFile(filenameComment);
      dataCom = JSON.parse(fileContents.toString());
    } catch (e) {
      dataCom = [];
    }
  },

  getCom(id) {
    return dataCom.find(p => p.idNews === id);
  },


  delCom(id){
    delete dataCom.splice(p => p.id === id,1);
    return this.saveCom();
  },

  addCom(Com) {
    if (Com.text){
      Com.id = nanoid();
      Com.date = new Date().toISOString();
      dataCom.push(Com);
      return this.saveCom();
    }
  },

  saveCom() {
    return fs.writeFile(filenameComment, JSON.stringify(dataCom));
  }
};