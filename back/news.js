const express = require('express');
const router = express.Router();
const data = require('./data');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('./config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const  upload = multer({storage});

router.get('/', (req, res) => {
  let news = data.getItems();
  res.send(news);
});

router.get('/:id', (req, res) => {
  const news = data.getItem(req.params.id);
  if (!news) {
    res.status(404).send({message: 'not found'})
  }
  return res.send(news);
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.text) {
      return res.status(404).send({message: 'text are required!'});
    }
    const news = {
      title: req.body.title,
      text: req.body.text,
    };

    if (req.file){
      news.image = req.file.filename;
    }

    await data.addItem(news);

    return res.send({message: 'created new message', id: news.id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', (req, res) => {
  const news = data.delItem(req.params.id);
  return res.send(news);
});


module.exports = router;