const express = require('express');
const app = express();
const data = require('./data');
const news = require('./news');
const comment = require('./comment');
const cors = require('cors');
const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.static('public'));
app.use(express.json());
app.use('/news', news);
app.use('/comment', comment);


const run = async () => {
  await data.init();
  await data.initCom();
}

app.listen(port, () => {
  console.log('We are live on ' + port);
});

run().catch(e => console.error(e));