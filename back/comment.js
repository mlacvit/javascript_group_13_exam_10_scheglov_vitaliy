const express = require('express');
const router = express.Router();
const data = require('./data');


router.get('/', (req, res) => {
  const comment = data.getCom(req.query.news);
  res.send(comment);
});

router.post('/', async (req, res, next) => {
  try {
    const com = {
      author: '',
      text: req.body.text,
      idNews: req.query.news,
    };

    if (req.body.author){
      com.author = req.body.author;
    }

    await data.addCom(com);

    return res.send({message: 'created new message', id: com.id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', (req, res) => {
  const com = data.delCom(req.params.id);
  return res.send(com);
});


module.exports = router;