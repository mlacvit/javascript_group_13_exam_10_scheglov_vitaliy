import { Injectable } from '@angular/core';
import { NewsData, NewsModel } from './newsmodal.module';
import { Subject, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { map } from 'rxjs/operators';
import { ComData, ComModel } from './com.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  news: NewsModel[] | null = null;
  newsOne: NewsModel | null = null;
  comment: ComModel[] | null = null;
  newsChange = new Subject<NewsModel[]>();
  newsChangeOne = new Subject<NewsModel[]>();
  comChange = new Subject<ComModel[]>();
  fetchNews = new Subject<boolean>();
  fetchCom = new Subject<boolean>();
  fetching = new Subject<boolean>();
  fetchingOne = new Subject<boolean>();
  fetchingCom = new Subject<boolean>();
  deleteUpLoading = new Subject<boolean>();
  deleteUpLoadingCom = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  createNews(NewsData: NewsData) {
    const formData = new FormData();
    Object.keys(NewsData).forEach(key => {
      if (NewsData[key] !== null) {
        formData.append(key, NewsData[key]);
      }
    });
    this.fetchNews.next(true);
    return this.http.post(environment.apiUrl + '/news', formData).pipe(
      tap(() => {
        this.fetchNews.next(false);
      }, error => {
        this.fetchNews.next(false);
      })
    );
  }

  getNews() {
    this.fetching.next(true);
    return this.http.get<NewsModel[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(com => {
          return new NewsModel(
            com.id,
            com.title,
            com.text,
            com.image,
            com.date,
          )
        });
      })
    ).subscribe({
      next: result => {
        this.news = result;
        this.newsChange.next(this.news.slice());
        this.fetching.next(false);
      },
      error: err => {
        this.fetching.next(false);
        console.log(err);
      }
    });
  }

  getNewsOne(id: string) {
    this.fetchingOne.next(true);
    return this.http.get<NewsModel>(environment.apiUrl + `/news/${id}`).pipe(
      map(result => {
          return new NewsModel(
            result.id,
            result.title,
            result.text,
            result.image,
            result.date,
          )
      })
    )
  }

  createCom(Com: ComData, id: string) {
    this.fetchCom.next(true);
    return this.http.post(environment.apiUrl + `/comment?news=${id}`, Com).pipe(
      tap(() => {
        this.fetchCom.next(false);
      }, error => {
        this.fetchCom.next(false);
      })
    );
  }

  getCom(id:string) {
    this.fetchingCom.next(true);
    return this.http.get<ComModel[]>(environment.apiUrl + `/comment`).pipe(
      map(response => {
        return response.map(news => {
          return new ComModel(
            news.id,
            news.author,
            news.text,
            news.date,
          )
        });
      })
    ).subscribe({
      next: result => {
        this.comment = result;
        this.comChange.next(this.comment.slice());
        this.fetchingCom.next(false);
      },
      error: err => {
        this.fetchingCom.next(false);
        console.log(err);
      }
    });
  }

  removeNews(id: string){
    this.deleteUpLoading.next(true);
    this.http.delete(environment.apiUrl + `/news/${id}`).subscribe(() => {
      this.deleteUpLoading.next(false);
      this.getNews();
    })
  };

  removeCom(id: string){
    this.deleteUpLoadingCom.next(true);
    this.http.delete(environment.apiUrl + `/comment/${id}`).subscribe(() => {
      this.deleteUpLoadingCom.next(false);
    })
  };

}
