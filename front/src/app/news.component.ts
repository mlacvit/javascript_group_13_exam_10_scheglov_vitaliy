import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NewsService } from './news.service';
import { NewsModel } from './newsmodal.module';
import { Subscription } from 'rxjs';
import { environment } from '../environments/environment';
import { NgForm } from '@angular/forms';
import { ComData, ComModel } from './com.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  idNews = '';
  news: NewsModel | null = null;
  comment: ComModel[] | null = null;
  changeSub!: Subscription;
  changeSubCom!: Subscription;
  fetchSub!: Subscription;
  fetchSubCom!: Subscription;
  fetchSubComRem!: Subscription;
  isFetching: boolean = false;
  isFetchingRem: boolean = false;
  isFetchingCom: boolean = false;
  apiUrl = environment.apiUrl;
  constructor(public service: NewsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.idNews = params['id'];
      this.service.getNewsOne(this.idNews).subscribe((one) => {
        this.news = one;
      });
    });
    this.service.getCom(this.idNews)
    this.changeSubCom = this.service.comChange.subscribe( (com: ComModel[]) => {
      this.comment = com;
    });

    this.fetchSubCom = this.service.fetchCom.subscribe((isFetching: boolean) => {
      this.isFetchingCom = isFetching;
    });

    this.fetchSubComRem = this.service.deleteUpLoadingCom.subscribe((isFetching: boolean) => {
      this.isFetchingRem = isFetching;
    });
  }

  onSubCom() {
    const value: ComData = this.form.value;
    this.service.createCom(value, this.idNews).subscribe(()=>{
      this.service.getCom(this.idNews);
    });
  }

  remCom(){
    this.service.removeCom(this.idNews);
  }

  ngOnDestroy(): void {
    this.changeSubCom.unsubscribe();
    this.fetchSubCom.unsubscribe();
    this.fetchSubComRem.unsubscribe();
  }
}
