import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { NewsModel } from '../newsmodal.module';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  news: NewsModel[] | null = null;
  changeSub!: Subscription;
  fetchSub!: Subscription;
  isFetching: boolean = false;
  apiUrl = environment.apiUrl;
  constructor(private service: NewsService) { }

  ngOnInit(): void {
    this.service.getNews();
    this.changeSub = this.service.newsChange.subscribe( (news: NewsModel[]) => {
      this.news = news;
    });

    this.fetchSub = this.service.fetching.subscribe( (isFech: boolean) => {
      this.isFetching = isFech;
    });
  }

  onRemote(id: string) {
    this.service.removeNews(id);
  }

  ngOnDestroy(): void {
    this.changeSub.unsubscribe();
    this.fetchSub.unsubscribe();
  };


}
