export class ComModel {
  constructor(
    public id: string,
    public author: string,
    public text: string,
    public date: string,
  ) {}
}

export interface ComData {
  idNews: string,
  title: string;
  text: string;
}
