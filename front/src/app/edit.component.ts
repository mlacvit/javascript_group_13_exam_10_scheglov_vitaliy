import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NewsData } from './newsmodal.module';
import { NewsService } from './news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  fetchingSubscriptions!: Subscription;
  isFetching: boolean = false;

  constructor(private service: NewsService, private routr: Router) { }

  ngOnInit(): void {

    this.fetchingSubscriptions = this.service.fetchNews.subscribe( (isFech: boolean) => {
      this.isFetching = isFech;
    });
  }

  onSub() {
    const value: NewsData = this.form.value;
    this.service.createNews(value).subscribe(()=>{
      this.service.getNews();
      this.routr.navigate(['/']);
    });
  };


  ngOnDestroy(): void {
    this.fetchingSubscriptions.unsubscribe();
  };
}
