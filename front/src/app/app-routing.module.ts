import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit.component';
import { NewsComponent } from './news.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new', component: EditComponent},
  {path: ':id', component: NewsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
