export class NewsModel {
  constructor(
    public id: string,
    public title: string,
    public text: string,
    public image: string,
    public date: string,
  ) {}
}

export interface NewsData {
  [key: string]: any;
  title: string;
  text: string;
  image: File | null;
}
